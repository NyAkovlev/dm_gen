import 'package:example/dependency/db.dart';

class GenericBloc<T> {
  final DB db;
  final List<T> items;

  GenericBloc(this.db, this.items);
}
