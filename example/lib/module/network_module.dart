import 'package:dm/dm.dart';
import 'package:example/dependency/network.dart';

class NetworkModule {
  @Provide()
  Network<String> networkString() => Network<String>();

  @Provide(singleton: false)
  Network<int> networkInt() => Network<int>();
}
