// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// ManagerGenerator
// **************************************************************************

import 'package:example/module/cache_module.dart';
import 'package:example/module/db_module.dart';
import 'package:example/module/network_module.dart';
import 'package:example/dm.dart';
import 'package:example/dependency/cache.dart';
import 'package:example/dependency/db.dart';
import 'package:example/dependency/network.dart';
import 'dart:core';
import 'package:example/bloc/common_consumer.dart';
import 'package:example/bloc/mark_inject_consumer.dart';
import 'package:example/bloc/with_parameter_consumer.dart';

class DmImpl extends Dm {
  final CacheModule _cacheModule;
  final DbModule _dbModule;
  final NetworkModule _networkModule;

  DmImpl({
    CacheModule cacheModule,
    DbModule dbModule,
    NetworkModule networkModule,
  })  : this._cacheModule = cacheModule ?? CacheModule(),
        this._dbModule = dbModule ?? DbModule(),
        this._networkModule = networkModule ?? NetworkModule();

  Cache _cache;
  DB _dB;
  Network<String> _networkString;

  Future<Dm> init() async {
    _dB = await _dbModule.db();
    return this;
  }

  Cache get _getCache =>
      _cache ??= _cacheModule.cacheModule(_getDB, _getNetworkString);
  DB get _getDB => _dB;
  Network<String> get _getNetworkString =>
      _networkString ??= _networkModule.networkString();
  Network<int> get _getNetworkint => _networkModule.networkInt();

  DB db() => _getDB;
  CommonConsumer commonConsumer() => CommonConsumer(
        _getDB,
      );
  MarkInjectConsumer markInjectConsumer() => MarkInjectConsumer.cached(
        _getDB,
        _getCache,
      );
  WithParameterConsumer withParameterConsumer(
    String name,
  ) =>
      WithParameterConsumer(
        _getDB,
        _getCache,
        name,
      );
}
