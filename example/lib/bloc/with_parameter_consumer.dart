import 'package:example/dependency/cache.dart';
import 'package:example/dependency/db.dart';

class WithParameterConsumer {
  final DB db;
  final Cache cache;
  final String name;

  WithParameterConsumer(this.db, this.cache, this.name);
}
