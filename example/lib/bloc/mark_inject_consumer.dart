import 'package:dm/dm.dart';
import 'package:example/dependency/cache.dart';
import 'package:example/dependency/db.dart';

class MarkInjectConsumer {
  final DB db;
  final Cache cache;

  @inject
  MarkInjectConsumer.cached(this.db, this.cache);
}
