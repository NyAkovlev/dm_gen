import 'package:dm/dm.dart';
import 'package:example/bloc/common_consumer.dart';
import 'package:example/bloc/mark_inject_consumer.dart';
import 'package:example/bloc/with_parameter_consumer.dart';
import 'package:example/dm.dm.dart';

import 'dependency/db.dart';
import 'module/cache_module.dart';
import 'module/db_module.dart';
import 'module/network_module.dart';

@Modules([CacheModule, DbModule, NetworkModule])
abstract class Dm {
  static Future<Dm> init() {
    return DmImpl().init();
  }

  @getInstance
  DB db();

  CommonConsumer commonConsumer();

  MarkInjectConsumer markInjectConsumer();

  WithParameterConsumer withParameterConsumer(String name);
}
