import 'package:example/dependency/db.dart';
import 'package:example/dependency/network.dart';

//mock
class Cache {
  final DB db;
  final Network<String> network;

  Cache(this.db, this.network);

  Future<List<String>> getData() async {
    try {
      final data = await network.getData();
      await db?.setData(data);
      return data;
    } catch (_) {
      if(db==null){
        return [];
      }
      return db.getData() ;
    }
  }
}
