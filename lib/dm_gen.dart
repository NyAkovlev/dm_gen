import 'package:build/build.dart';
import 'package:dm_gen/src/generator.dart';
import 'package:source_gen/source_gen.dart';

Builder builder([BuilderOptions? options]) =>
    LibraryBuilder(ManagerGenerator(), generatedExtension: ".dm.dart");
