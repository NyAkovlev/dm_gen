import 'package:analyzer/dart/constant/value.dart';
import 'package:analyzer/dart/element/element.dart';
import 'package:analyzer/dart/element/nullability_suffix.dart';
import 'package:analyzer/dart/element/type.dart';
import 'package:dm/dm.dart';
import 'package:dm_gen/src/type_util.dart';
import 'package:dm_gen/src/util.dart';
import 'package:source_gen/source_gen.dart';

class Dependency {
  final Set<String> import;
  final String module;
  final String typeName;
  final bool nullable;
  final List<String> parameters;
  final String function;
  final bool isSingleton;
  final bool isFuture;

  Dependency(
    this.module,
    this.typeName,
    this.parameters,
    _isSingleton,
    this.isFuture,
    this.function,
    this.import,
    this.nullable,
  ) : isSingleton = !_isSingleton && isFuture ? true : _isSingleton {
    if (!_isSingleton && isFuture) {
      addLog(
          "not singleton future is not supported\n$typeName singleton forcibly");
    }
  }

  static Dependency fromMethod(
    ClassElement module,
    MethodElement method,
    DartObject annotation,
      bool nullSafety,
  ) {
    var returnType = method.returnType;
    var isFuture = false;
    final isSingleton = annotation.getField("singleton")?.toBoolValue() ?? false;
    if (returnType.isDartAsyncFuture) {
      isFuture = true;
      returnType = (returnType as InterfaceType).typeArguments.first;
    }
    final imports = {returnType.element!.library!.librarySource.uri.toString()};
    getGenericTypeImports(returnType, imports);
    return Dependency(
      module.name,
      returnType.getDisplayString(withNullability: nullSafety),
      method.parameters.map((item) {
        return item.type.getDisplayString(withNullability: nullSafety);
      }).toList(),
      isSingleton,
      isFuture,
      method.name,
      imports,
      returnType.nullabilitySuffix == NullabilitySuffix.question,
    );
  }
}
