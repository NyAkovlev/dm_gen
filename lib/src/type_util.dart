import 'package:analyzer/dart/element/type.dart';

getGenericTypeImports(DartType type, Set<String> imports) {
  if (type is InterfaceType) {
    type.typeArguments.forEach((item) {
      imports.add(item.element!.librarySource!.uri.toString());
      getGenericTypeImports(item, imports);
    });
  }
}
