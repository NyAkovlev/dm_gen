import 'package:dm_gen/src/error/base_error.dart';

class NotHaveConstructor extends BaseError {
  final String name;

  NotHaveConstructor(this.name);

  @override
  String message() {
    return "Class $name does not have a constructor";
  }
}
