import 'package:dm_gen/src/error/base_error.dart';
import 'package:dm_gen/src/model/dependency.dart';

class ModuleCanNotReturnNullableType extends BaseError {
  final Dependency dependency;

  ModuleCanNotReturnNullableType(this.dependency);

  @override
  String message() {
    return "Module ${dependency.module} return nullable type ${dependency.typeName}";
  }
}
