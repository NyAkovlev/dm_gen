import 'package:dm_gen/src/error/base_error.dart';

class ModuleHaveInvalidConstructor extends BaseError{
  final String name;

  ModuleHaveInvalidConstructor(this.name);
  @override
  String message() {
    return "Module $name must have a constructor without parameters";
  }

}