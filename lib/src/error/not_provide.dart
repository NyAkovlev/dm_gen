import 'package:dm_gen/src/error/base_error.dart';

class NotProvide extends BaseError {
  final String name;
  final String? forType;

  NotProvide(this.name, [this.forType]);

  @override
  String message() {
    if (forType != null) {
      return "type $name is not provided for $forType";
    }
    return "type $name is not provided";
  }
}
