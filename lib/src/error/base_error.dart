abstract class BaseError extends Error {
  String message();

  @override
  String toString() {
    return runtimeType.toString() + wrap(message());
  }

  static wrap(String message) {
    return """ __________________________

    ${message}

    _______________________________
    """;
  }
}
