import 'package:analyzer/dart/element/element.dart';
import 'package:build/src/builder/build_step.dart';
import 'package:dm/dm.dart';
import 'package:dm_gen/src/content/content_builder.dart';
import 'package:dm_gen/src/error/module_Is_abstract.dart';
import 'package:dm_gen/src/error/module_have_invalid_constructor.dart';
import 'package:dm_gen/src/error/not_provide.dart';
import 'package:dm_gen/src/model/consumer.dart';
import 'package:dm_gen/src/model/dependency.dart';
import 'package:dm_gen/src/util.dart';
import 'package:source_gen/source_gen.dart';

import 'error/module_can_not_return_nullable_type.dart';

class ManagerGenerator extends GeneratorForAnnotation<Modules> {
  late bool nullSafety;

  @override
  generateForAnnotatedElement(
    Element element,
    ConstantReader annotation,
    BuildStep buildStep,
  ) {
    try {
      assert(element is ClassElement);
      nullSafety = element.library!.isNonNullableByDefault;
      final modules = getModules(annotation);

      final imports = getImports(modules);
      imports.add(element.library!.librarySource.uri.toString());

      final dependency = getDependency(modules);
      final consumers = getConsumers(element as ClassElement);
      checkDependency(dependency, consumers);

      final contentBuilder = ContentBuilder(
        element.name,
        dependency,
        modules,
        imports,
        consumers,
        nullSafety,
      );
      final out = contentBuilder.build();
      printLog();
      return out;
    } catch (e, s) {
      e;
      s;
      rethrow;
    }
  }

  List<ClassElement> getModules(ConstantReader annotation) {
    return annotation.objectValue.getField("modules")?.toListValue()?.map(
          (item) {
            final module = item.toTypeValue()!.element as ClassElement;

            if (module.isAbstract) {
              throw ModuleIsAbstract(module.name);
            }
            if (module.constructors.first.parameters.isNotEmpty) {
              throw ModuleHaveInvalidConstructor(module.name);
            }

            return module;
          },
        ).toList() ??
        [];
  }

  List<String> getImports(List<ClassElement> modules) {
    final imports = <String>[];
    for (var module in modules) {
      imports.add(module.library.librarySource.uri.toString());
    }
    return imports;
  }

  List<Dependency> getDependency(List<ClassElement> modules) {
    final dependencies = <Dependency>[];

    for (var module in modules) {
      for (var method in module.methods) {
        final annotation = _providerChecker.firstAnnotationOf(method);
        if (annotation != null) {
          dependencies.add(Dependency.fromMethod(module, method, annotation,nullSafety));
        }
      }
    }
    return dependencies;
  }

  List<Consumer> getConsumers(ClassElement element) {
    final consumers = <Consumer>[];
    for (var method in element.methods) {
      if (method.isAbstract) {
        consumers.add(Consumer.fromMethod(element, method,nullSafety));
      }
    }
    return consumers;
  }

  checkDependency(List<Dependency> dependencies, List<Consumer> consumers) {
    final checked = <Dependency>[];
    final existTypes = <String>{};
    var lastCheckResult = -1;
    String? requiredParameter = null;
    var nextCheckDependencies = dependencies.toList();
    while (
        lastCheckResult != checked.length && nextCheckDependencies.isNotEmpty) {
      final currentCheck = nextCheckDependencies;
      nextCheckDependencies = nextCheckDependencies.toList();
      lastCheckResult = checked.length;

      for (var dependency in currentCheck) {
        requiredParameter = dependency.parameters.firstWhereOrNull(
          (parameter) {
            return !existTypes.contains(parameter);
          },
        );

        if (requiredParameter == null) {
          if (dependency.nullable) {
            throw ModuleCanNotReturnNullableType(dependency);
          }
          existTypes.add(dependency.typeName);
          checked.add(dependency);
          nextCheckDependencies.remove(dependency);
        }
      }
    }

    if (requiredParameter != null) {
      throw NotProvide(requiredParameter);
    }

    for (var consumer in consumers) {
      if (consumer.fromProvide) {
        if (!existTypes.contains(consumer.typeName)) {
          throw NotProvide(consumer.typeName);
        }
      } else {
        final types = existTypes.toSet()..addAll(consumer.parameterTypes);

        final requiredParameter = consumer.initParameterTypes.firstWhereOrNull(
          (parameter) {
            return !types.contains(parameter);
          },
        );

        if (requiredParameter != null) {
          throw NotProvide(requiredParameter, consumer.typeName);
        }
      }
    }
  }

  static const _providerChecker = TypeChecker.fromRuntime(Provide);
}
